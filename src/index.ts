// création d'un type dimension
type dimensions = {
  length: number;
  width: number;
  height: number;
};

// création d'un type adresse
type adress = {
  street: string;
  city: string;
  postalCode: string;
  country: string;
};

// création class customer
class customer {
  customerId: number;
  name: string;
  email: string;
  adress?: adress; // ajout de la propriété adress sans modifier le constructeur. cette propriété est donc optionnelle(pas dans le constructor)

  // Constructeur pour créer une nouvelle instance de la class
  constructor(customerId: number, name: string, email: string) {
    this.customerId = customerId;
    this.name = name;
    this.email = email;
  }
  // Méthode  displayInfo: cette méthode va retourner les informations de la class customer
  displayInfo(): string {
    // on crée une variable pour stocker le retour de la méthode displayAdress et le rajouter dans display info
    const adressDetail = this.displayAddress();
    return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Adress:${adressDetail}`;
  }

  //Méthode SetAdress: cette méthode permet de renseigner l'adresse
  // type void :  cela signifie que la fonction ne renvoie aucune valeur
  setAdress(adress: adress): void {
    this.adress = adress;
  }

  // Méthode :cette méthode va retourner les informations de l'adresse
  displayAddress(): string {
    //si on a une adress on retourne l'adresse sinon on retourne une phrase "no adress found"
    if (this.adress) {
      const { street, postalCode, city, country } = this.adress;
      return `${street}, ${postalCode} ${city}, ${country}`;
    } else {
      return "No address found";
    }
  }
}

// création class product
class product {
  productId: number;
  name: string;
  weight: number;
  price: number;
  dimensions: dimensions; //type dimensions cf ligne2

  //constructeur pour créer une nouvelle instance de la classe
  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimensions: dimensions 
  ) {
    this.productId = productId;
    this.name = name;
    this.weight = weight;
    this.price = price;
    this.dimensions = dimensions; 
  }

  //méthode DisplayDetails: cette méthode va retourner les informations de la classe product
  displayDetails(): string {
    // on stock les dimensions dans une variable pour les retourner au bon format
    const dimensionsDetails = this.dimensionsToString();
    return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price} €, Dimensions :${dimensionsDetails} `;
  }

  // on crée une méthode ToString pour afficher les dimensions au bon format
  dimensionsToString(): string {
    return `Length: ${this.dimensions.length}, Width: ${this.dimensions.width}, Height: ${this.dimensions.height}`;
  }
}

// Utilisation des classes
//customer
const customer1 = new customer(
  1,
  "Claire Kondrakhin",
  "claireKondrakhin@example.com"
);
const adressCustomer1: adress = {
  street: "22 rue de la pomme",
  postalCode: "34000",
  city: "Montpellier",
  country: "France",
};

const customer2 = new customer(
  2,
  "Marie Evian",
  "M.evian@example.com"
);
const adressCustomer2: adress = {
  street: "50 rue montorgueil",
  postalCode: "75002",
  city: "Paris",
  country: "France",
};


// avec la variable adressCustomer1 j'appelle la méthode setAdress dans le customer1
customer1.setAdress(adressCustomer1);
//j'appelle la méthode displayInfo pour le customer1
console.log(customer1.displayInfo());
// renvoi :Customer ID: 1, Name: Claire Kondrakhin, Email: claireKondrakhin@example.com, Adress:22 rue de la pomme, 34000 Montpellier, France

//Dimension produit 1
const product1Dimension: dimensions = {
  length: 50,
  width: 30,
  height: 70,
};

const product1 = new product(1, "robe noire", 38, 50, product1Dimension);
console.log(product1.displayDetails());
//renvoi : Product ID: 1, Name: robe noire, Weight: 38, Price: 50 €, Dimensions :Length: 50, Width: 30, Height: 70

// déclaration enum ClothingSize et ShoeSize
enum ClothingSize {
  XS = "XS",
  S = "S",
  M = "M",
  L = "L",
  XL = "XL",
  XXL = "XXL",
}

enum ShoeSize {
  Size36 = 36,
  Size37 = 37,
  Size38 = 38,
  Size39 = 39,
  Size40 = 40,
  Size41 = 41,
  Size42 = 42,
  Size43 = 43,
  Size44 = 44,
  Size45 = 45,
  Size46 = 46,
}

// création de sous classe clothing et shoes

class clothing extends product {
  size: ClothingSize;
  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimensions: dimensions,
    size: ClothingSize
  ) {
    super(productId, name, weight, price, dimensions);
    this.size = size;
    }
    
    // méthode pour afficher les détails du produit (classe parent) et les propriété de clothing (classe enfant)
  displayDetails(): string {
    return `${super.displayDetails()}, Size: ${this.size}`;
  }
}

class shoes extends product {
  size: ShoeSize;
  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    dimensions: dimensions,
    size: ShoeSize
  ) {
    super(productId, name, weight, price, dimensions);
    this.size = size;
    }
    // méthode pour afficher les détails du produit (classe parent) et les propriété de shoes (classe enfant)
    
  displayDetails(): string {
    return `${super.displayDetails()}, Size: ${this.size}`;
  }
}

// Utilisation des sous-classes
const product2 = new clothing(
  2,
  "T-shirt",
  0.2,
  20,
  { length: 10, width: 8, height: 1 },
  ClothingSize.M
);
console.log(product2.displayDetails());

const product3 = new shoes(
  3,
  "bottes",
  0.5,
  50,
  { length: 12, width: 4, height: 5 },
  ShoeSize.Size42
);
console.log(product3.displayDetails());

// création classe order

class order {
  orderId: number;
  customer: customer;
  productList: product[];
  orderDate: Date;
  delivery?: Deliverable; // Ajout de la propriété delivery sans modifier le constructeur

  constructor(
    orderId: number,
    customer: customer,
    productList: product[],
    orderDate: Date
  ) {
    this.orderId = orderId;
    this.customer = customer;
    this.productList = productList;
    this.orderDate = orderDate;
  }

  //méthode pour ajouter un produit à la commande
  addProduct(product: product): void {
    this.productList.push(product);
  }

  // méthode pour retirer un produit de la commande en utiliant filter.
  //filter crée un nouveau tableau excluant l'élément dont l'identifiant (productId).
  //Ensuite, je réaffecte ce nouveau tableau à la propriété productList
  removeProduct(productId: number): void {
    this.productList = this.productList.filter(
      (item) => item.productId !== productId
    );
  }
  // méthode pour calculer le poids total de la commande en utilisant la méthode reduce
  calculateWeight(): number {
    return this.productList.reduce((sum, product) => sum + product.weight, 0);
  }

  //Méthode pour calculer le prix total de la commande : calculateTotal()
  calculateTotal(): number {
    return this.productList.reduce((sum, product) => sum + product.price, 0);
  }

  //Méthode pour afficher les détails de la commande : les informations de l'utilisateur, les informations
  //de chaque produit et le total de la commande. displayOrder()

  displayOrder(): string {
    // on stocke les infos du clients dans une variable
    const customerInfo = this.customer.displayInfo();
    // on stock les détails des produits de la commande
    // on utilise la méthode map pour appliquer la fonction displayDetails() à chaque élément de la liste des produits
    const productDetails = this.productList.map((product) =>
      product.displayDetails()
    );
    // on crée une variable qui stock le résultat de la méthode calculateTotal
    const total = this.calculateTotal();

    return `Order ID: ${this.orderId}, Customer Info: ${customerInfo}, Products Ordered:${productDetails}, Total Order Price: ${total} €`;
  }

  // Ajout de la méthode qui permet d'assigner un service de livraison à la commande.
  setDelivery(delivery: Deliverable): void {
    this.delivery = delivery;
  }
  // méthode pour estimer le délai de livraison en nombre de jours
    estimateDeliveryTime(weight: number): number {
          if (this.delivery) {
            const totalWeight = this.calculateWeight();
            return this.delivery.estimateDeliveryTime(totalWeight);
          } else {
            return 0; // Aucune estimation si la livraison n'est pas définie
          }
  }
  //méthode pour calculer les frais de livraison
    calculateShippingFee(weight: number): number {
          if (this.delivery) {
            const totalWeight = this.calculateWeight();
            return this.delivery.calculateShippingFee(totalWeight);
          } else {
            return 0; // Aucun frais si la livraison n'est pas définie
          }
  }
}

// Interface Deliverable
interface Deliverable {
  estimateDeliveryTime(weight: number): number;
  calculateShippingFee(weight: number): number;
}

//creation des classes StandardDelivery et ExpressDelivery
class StandardDelivery implements Deliverable {
    //methode pour estimer le delai de livraison 
    estimateDeliveryTime(weight: number): number {
        //livraison standard
        //si poids < 10, on retourne 7
        // sinon on retourne 10
        if (weight < 10) {
            return 7;
        } else {
            return 10;
        }
    }

    // méthode pour calculer les frais de livraison
    calculateShippingFee(weight: number): number {
        //livraison standard
        //si poids < 1, on retourne 5
        // si poids entre 1 et 5kg, on retourne 10
        // si >5kg on retourne 20
        if (weight < 1) {
            return 1 
        } else if (weight >= 1 && weight <= 5) {
            return 10 ;
        } else {
            return 20;
        } 
    }
}
class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        // livraison express
        // si poids <=5, on retourne 1
        //sinon, on retourne 3
        if (weight <= 5) {
            return 1;
        } else {
            return 3;
        }
    }

    calculateShippingFee(weight: number): number {
        // livraison express
        //si poids < 1, on retourne 8
        // si poids entre 1 et 5kg, on retourne 14
        // si >5kg on retourne 30
        if (weight < 1) {
            return 8;
        } else if (weight >= 1 && weight <= 5) {
            return 14;
        } else {
            return 30;
        }
    }
}

// exemple pour order
const orderDate = new Date();
const order1 = new order(1, customer1, [product1], orderDate);
const order2 = new order(2, customer1, [product2, product3], orderDate);
const order3 = new order(3, customer1, [product1, product3], orderDate);
const standardDelivery = new StandardDelivery(); // Création d'une instance de StandardDelivery
order3.setDelivery(standardDelivery);

// Calcul du poids total de la commande order1
const totalWeight = order1.calculateWeight();
console.log(`Poids total de la commande: ${totalWeight} kg`); // Poids total de la commande 1: 38 kg

//calcul du prix total de la comande
const totalPrice = order2.calculateTotal();
console.log(`Prix total de la commande: ${totalPrice} €`); //Prix total de la commande 2 : 70€

// Afficher le détail d'une commande
console.log(order2.displayOrder()); // Order ID: 2, Customer Info: Customer ID: 1, Name: Claire Kondrakhin, Email: claireKondrakhin@example.com, Adress:22 rue de la pomme, 34000 Montpellier, France, Products Ordered:Product ID: 2, Name: T-shirt, Weight: 0.2, Price: 20 €, Dimensions :Length: 10, Width: 8, Height: 1 , Size: M,Product ID: 3, Name: bottes, Weight: 0.5, Price: 50 €, Dimensions :Length: 12, Width: 4, Height: 5 , Size: 42, Total Order Price: 70 €

// Calcul des frais de livraison de la commande 3
console.log(order3.calculateWeight()) // retourne 38.5 (kg)
console.log(order3.calculateShippingFee(totalWeight)) // retourne 20 (€)
console.log(order3.estimateDeliveryTime(totalWeight)) // retourne 10 (days)


